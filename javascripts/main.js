const sumIncomeResult = document.querySelector(".sum-in-month");
const sumCostResult = document.querySelector(".cost-money");
const range = document.getElementById("range");
const rangeResult = document.querySelector(".range-moneybox-percent");
const moneySaveResult = document.querySelector(".money-save-result");
const labelInDay = document.querySelector(".in-day");
const saveInYear = document.querySelector(".save-in-year");

const incomeArr = [
  {
    id: "field1",
    value: 0,
  },
  {
    id: "field2",
    value: 0,
  },
  {
    id: "field3",
    value: 0,
  },
  {
    id: "field4",
    value: 0,
  },
];

const costArr = [
  {
    id: "cost1",
    value: 0,
  },
  {
    id: "cost2",
    value: 0,
  },
  {
    id: "cost3",
    value: 0,
  },
  {
    id: "cost4",
    value: 0,
  },
];

let incomeInputWrapper = document.querySelector(".income-input");
let costInputWrapper = document.querySelector(".cost-input");
let btnInputCreate = document.querySelector(".calc-btn-input-create");
let incomeTotal = 0;
let costTotal = 0;
let saveResult = 0;
let costResult = 0;
let avaiResult = 0;
let diffSumResult = 0;

//Events
document.addEventListener("DOMContentLoaded", function () {
  renderIncomeInputs(incomeArr);
  renderCostInputs(costArr);
});

btnInputCreate.addEventListener("click", function (e) {
  const nextIndex = incomeArr.length + 1;
  const inputObject = {
    id: `field${nextIndex}`,
    value: 0,
  };
  incomeArr.push(inputObject);
  renderIncomeInputs(incomeArr);
});

btnInputCreate.addEventListener("click", function (e) {
  const nextIndex = costArr.length + 1;
  const inputObject = {
    id: `cost${nextIndex}`,
    value: 0,
  };
  costArr.push(inputObject);
  renderCostInputs(costArr);
});

range.addEventListener("change", function (e) {
  getMoneySave();
  getSaveMoneyInYear();
  getMoneyInDay();
});

document.addEventListener("DOMContentLoaded", function (e) {
  range.value = 0;
});

//Functions
function incomeInputCreate() {
  let input = document.createElement("input");
  input.setAttribute("type", "number");
  let parent = document.querySelector(".income-input");
  parent.appendChild(input);
}

function costInputCreate() {
  let input = document.createElement("input");
  input.setAttribute("type", "number");
  let parent = document.querySelector(".cost-input");
  parent.appendChild(input);
}

function renderIncomeInputs(fields) {
  incomeInputWrapper.innerHTML = "";
  fields.forEach((field, index) => {
    const input = createIncomeInput(field, index);
    incomeInputWrapper.appendChild(input);
  });
}

function renderCostInputs(fields) {
  costInputWrapper.innerHTML = "";
  fields.forEach((field, index) => {
    const input = createCostInput(field, index);
    costInputWrapper.appendChild(input);
  });
}

function createIncomeInput(field, index) {
  const input = document.createElement("input");
  input.setAttribute("type", "number");
  input.setAttribute("id", field.id);
  input.dataset.index = index;
  input.setAttribute("value", field.value);
  input.addEventListener("change", handleIncomeInput);

  return input;
}

function createCostInput(field, index) {
  const input = document.createElement("input");
  input.setAttribute("type", "number");
  input.setAttribute("id", field.id);
  input.dataset.index = index;
  input.setAttribute("value", field.value);

  input.addEventListener("change", handleCostInput);

  return input;
}

function handleIncomeInput(e) {
  const inputIndex = e.target.dataset.index;
  const inputValue = e.target.value;
  incomeArr[inputIndex].value = parseInt(inputValue);
  getSumIncome();
  getSumCost();
  getDiffSum();
  getRangeValue();
  getMoneySave();
  getMoneyInDay();
  getSaveMoneyInYear();
}

function handleCostInput(e) {
  const inputIndex = e.target.dataset.index;
  const inputValue = e.target.value;
  costArr[inputIndex].value = parseInt(inputValue);
  getSumIncome();
  getSumCost();
  getDiffSum();
  getRangeValue();
  getMoneySave();
  getMoneyInDay();
  getSaveMoneyInYear();
}

function getSumIncome() {
  incomeTotal = incomeArr.reduce(function (a, b) {
    return a + b.value;
  }, 0);
}

function getSumCost() {
  costTotal = costArr.reduce(function (a, b) {
    return a + b.value;
  }, 0);
}

function getRangeValue() {
  if (diffSumResult <= 0) {
    range.value = 0;
  }
  const rangeValue = range.value;
  renderHtml(rangeResult, rangeValue + "%");
}

function getMoneySave() {
  const rangeValue = range.value;
  saveResult = (diffSumResult * rangeValue) / 100;
  costResult = costTotal + saveResult;

  renderHtml(moneySaveResult, saveResult);
  renderHtml(sumCostResult, getRound(costResult));
}

function getMoneyInDay() {
  const year = new Date().getFullYear();
  const month = new Date().getMonth();
  const days = new Date(year, month, 0).getDate();
  avaiResult = (incomeTotal - costResult) / parseInt(days);
  renderHtml(labelInDay, getRound(avaiResult));
}

function getSaveMoneyInYear() {
  const saveResultInYear = saveResult * 12;
  renderHtml(saveInYear, getRound(saveResultInYear));
}

function getDiffSum() {
  diffSumResult = incomeTotal - costTotal;
  renderHtml(sumIncomeResult, diffSumResult);
}

//Helper functions
function renderHtml(where, what) {
  where.innerHTML = what;
}

function getRound(value) {
  return Math.round(value);
}
